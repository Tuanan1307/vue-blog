import axios from "axios";

const API = axios.create({ baseURL: "http://localhost:3000" });

export const getBlogList = () => {
  return API.get("/blogs");
};
export const createBlogList = (data) => {
  return API.post("/blogs", data);
};
export const deleteBlogList = (id) => {
  return API.delete(`/blogs/${id}`);
};
export const updateBlogList = (id, newData) => {
  return API.put(`/blogs/${id}`, newData);
};
export const getBlogListById = (id) => {
  return API.get(`/blogs/${id}`);
};
export const getBlogListBySearch = (search) => {
  return API.get(`/blogs?q=${search}`);
};
