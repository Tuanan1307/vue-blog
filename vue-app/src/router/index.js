import VueRouter from "vue-router";
import Vue from "vue";
import BlogList from "@/components/blog/BlogList.vue";
import BlogCreateEdit from "@/components/blog/BlogCreateEdit.vue";
import BlogSearchForm from "@/components/blog/BlogSearchForm.vue";
const routes = [
  { path: "/bloglist", component: BlogList, name: "bloglist" },
  { path: "/blog-create", component: BlogCreateEdit, name: "blogcreate" },
  { path: "/blog-edit/:id", component: BlogCreateEdit, name: "blogedit" },
  { path: "/blog-search", component: BlogSearchForm, name: "blogsearch" },
];
Vue.use(VueRouter);
const router = new VueRouter({
  routes,
  mode: "history",
});

export { router };
